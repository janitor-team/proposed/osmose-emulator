###############################################################################
#
# * This file is part of Osmose Emulator, a Sega Master System/Game Gear
# * software emulator.
# *
# * Many thanks to Vedder Bruno, the original author of Osmose Emulator.
# *
# * Copyright holder 2001-2011 Vedder Bruno.
# * Work continued by 2016-2020 Carlos Donizete Froes [a.k.a coringao]
# *
# * Osmose Emulator is free software: you can redistribute it and/or modify it
# * under the terms of the GNU General Public License as published by the
# * Free Software Foundation, either version 3 of the License,
# * or (at your option) any later version.
#
###############################################################################

TEMPLATE = app
TARGET = osmose-emulator
INCLUDEPATH += src
DEPENDPATH += src

QT += core gui opengl
QMAKE_CFLAGS += -Wmissing-prototypes -Wshadow $$FLAGS
QMAKE_CXXFLAGS += -g -std=gnu++17 $$FLAGS
QMAKE_LFLAGS += -fPIE -pie -Wl,--as-needed -Wl,-z,now

FLAGS += -Wall -Wextra -Wno-unused -Wcast-qual
LIBS += -lz -lasound -lGL -lGLU -lglut

# Output
OBJECTS_DIR += src
MOC_DIR += src
RCC_DIR += src
UI_DIR += src

# The following define makes your compiler warn you if you use any
# feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# Input
HEADERS += src/AnsiColorTerminal.h \
           src/BasicTypes.h \
           src/Bits.h \
           src/DebugEventListener.h \
           src/DebugEventThrower.h \
           src/Definitions.h \
           src/EmulationThread.h \
           src/FIFOSoundBuffer.h \
           src/IOMapper.h \
           src/IOMapper_GG.h \
           src/Joystick.h \
           src/KeyMapper.h \
           src/MemoryMapper.h \
           src/Options.h \
           src/OsmoseConfigurationFile.h \
           src/OsmoseCore.h \
           src/OsmoseEmulationThread.h \
           src/OsmoseGUI.h \
           src/Pthreadcpp.h \
           src/QGLImage.h \
           src/QLogWindow.h \
           src/QOsmoseConfiguration.h \
           src/RomSpecificOption.h \
           src/SaveState.h \
           src/SmsEnvironment.h \
           src/SN76489.h \
           src/SoundThread.h \
           src/TGAWriter.h \
           src/VDP.h \
           src/VDP_GG.h \
           src/Version.h \
           src/WaveWriter.h \
           src/WhiteNoiseEmulationThread.h \
           src/Z80.h
FORMS += src/Configuration.ui src/LogWindow.ui
SOURCES += src/DebugEventThrower.cpp \
           src/EmulationThread.cpp \
           src/FIFOSoundBuffer.cpp \
           src/IOMapper.cpp \
           src/IOMapper_GG.cpp \
           src/Joystick.cpp \
           src/KeyMapper.cpp \
           src/main.cpp \
           src/MemoryMapper.cpp \
           src/Opc_cbxx.cpp \
           src/Opc_dd.cpp \
           src/Opc_ddcb.cpp \
           src/Opc_ed.cpp \
           src/Opc_fd.cpp \
           src/Opc_fdcb.cpp \
           src/Opc_std.cpp \
           src/Options.cpp \
           src/OsmoseConfigurationFile.cpp \
           src/OsmoseCore.cpp \
           src/OsmoseEmulationThread.cpp \
           src/OsmoseGUI.cpp \
           src/Pthreadcpp.cpp \
           src/QGLImage.cpp \
           src/QLogWindow.cpp \
           src/QOsmoseConfiguration.cpp \
           src/RomSpecificOption.cpp \
           src/SmsEnvironment.cpp \
           src/SN76489.cpp \
           src/SoundThread.cpp \
           src/TGAWriter.cpp \
           src/VDP.cpp \
           src/VDP_GG.cpp \
           src/WaveWriter.cpp \
           src/WhiteNoiseEmulationThread.cpp \
           src/Z80.cpp
RESOURCES += src/OsmoseResources.qrc
